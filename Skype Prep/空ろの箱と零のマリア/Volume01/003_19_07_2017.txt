﻿page 104

異様 and 異常 same meaning?
異様
異常 - more basic

page 105

悟る -　Same as 気付いている？
basically same but satoru is more 'cool; - old word

考えるまでもない one word?
...mademonai - mean we dont need to (verb)

途方もない時間を過ごし、手にした物がこんな結果だとはな。
手にした物 - here means what I did by my own hands?
my doing - similar to my fault but not negative, only result

page 106

闘いと戦いの違いは何ですか？
闘い - mean i fight, but i am defend but not self defense
戦い = mean basic

page 108

そもそもね、そういうマイナスの付加価値を付けてしまうのは、いつだってその道具の特性ではなく、使う人間の性質のせいなんだ。
why minus added value?
minus here means bad, like warui

遠慮　- third person - other ppl want to do so i dont do it
我慢 - one want to do but one does not do

page 109

価値、value but not cost, like how important
値段、value as in cast - direct
価格 value as in "about" order of 1000 but basically same
    like how cheap or expensive (value, position)

page 110

ザリ、ザリ、ザリ、ザリ――　what noise?
sound like sandpaper, rough etc
ざらざら　is similar, but like sand, small paper
つるつる is smooth

擬音語 = onomatopeia
擬態語　＝ mimic what it would sound like

擬声語 = parent set of these sound words

聞き逃す is this one word?
fail to hear eg, fail to hear cos thinking

内側、うちがわ - inner wall of the box, sometimes inner space
中身、なかみ = this is the small box
内容、contents, eg information - inside book = the story
内部　—　ないぶ space of big box (if box inside box, it is the big one)

小さな小さなヤスリを掛けられている。
what is yasuri?
The tool - file used in metalworking

尚更 = まだ ?
尚更 basically use hiragana なおさら
want to eat more なおさら食べたい
まだ...ない

音が余計に聞こえる　And　音が聞こえすぎる　same?
basically same but 余計 implies there is a target
you can hear target sound, and more
2nd one only i can hear alot

これって罪悪感？そんなもの、一番初めに失うと思っていたのに存外しぶとい。
what is zhongai shibutoi?
存外 - without my thinking
しぶとい - similar to stubborn but not opinion
person do 諦めない so person is shibutoi

in Lol if adc easy to die but has lifesteal he is 存外しぶとい。

page 111

もう誰ともまともにコミュニケーションも取れない

まとも　－真面 meaning like face to face, same room etc, not over phone
