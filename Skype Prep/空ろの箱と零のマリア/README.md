# 空ろの箱と零のマリア

Around 15th April 2017, I decided that my vocabulary was lacking and in order to try aid that, I decided to start reading a light novel, called 空ろの箱と零のマリア. I am hoping that by reading more literature, it should help me expand my vocabulary and aid my grammar too. My course of action is to read the book and any words or expressions I come across that I don't understand, I will mark it down and search for the meaning myself and if I still don't understand, I will note it down and ask my friend, Yuji during the weekly Skype call.

Currently, I take around 1 hour to finish one page, as there are many, many words I do not understand.

<https://myanimelist.net/manga/55215/Utsuro_no_Hako_to_Zero_no_Maria>
