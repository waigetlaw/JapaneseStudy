単語 | 発音 | 意味
---|---|---
出鱈目|でたらめ|nonsense; hogwash; rubbish // random; haphazard; unsystematic
荒唐無稽|こうとうむけい|absurd; nonsensical; preposterous
朝飯前|あさめしまえ|trivial matter; easy as pie; piece of cake
ロクデナシ|ろくでなし| bum; good-for-nothing
彼岸花|ひがんばな|red spider lily
深淵|しんえん|abyss; ravine
晩餐|ばんさん|dinner
面影|おもかげ|face; looks; vestiges; trace​
揺り籠|ゆりかご|cradle
手招き|てまねき|beckoning
迷宮|めいきゅう|labrynth
贖い|あがない|atonement; redemption; compensation