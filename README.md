# Japanese Studying Material

My name is Wai Get Law, currently 25, and I started studying back in October 2016 by simply listening to audiobooks or Youtube videos during nightshift and have now permanently incorporated Japanese Study into my daily schedule. My current schedule is a weekly 1-hour Skype call with a Japanese friend, handwriting practice from Monday to Friday and Reading practice during the weekend. This is just the minimum I do, and I always do some extra research on things I find interesting, or past points that needs revising. I also love watching anime but I do not consider that as a form of studying, although I do consider reading light novels as studying.  

## Goals

+ Visit Japan - Achieved 9th August 2017
+ My first goal is to obtain a pass on any level of the Japanese Language Proficiency Test (JLPT) - Achieved 3rd December 2017 (N3)
+ Become confident in all grammar rules and conjugations
+ Obtain a level N2 pass on JLPT
+ Obtain a level N1 pass on JLPT
+ Able to read 80% of Japanese literature with little use of dictionary
+ Able to speak fluently in Japanese
+ Able to watch videos in Japanese and understand
+ Able to write in Japanese to a decent level

## Resources

Here is a list of all resources I have used for my studies:

### Tools

+ Rikaikun - This is a Chrome add-on that can translate Japanese text on websites with meanings and readings, super useful!  
<https://chrome.google.com/webstore/detail/rikaikun/jipdnfibhldikgcjhfnomkfpcebammhp?hl=en>  

### Websites

<https://www.youtube.com/user/yesjapan>  
<http://www.guidetojapanese.org/learn/>  
<https://www.reddit.com/r/LearnJapanese/>  
<http://jisho.org/>  
<http://www.tanoshiijapanese.com/dictionary/>  
<http://www.jlpt.jp/e/application/overseas_list.html>  
<https://www.nhk.or.jp/lesson/english/>   
<https://translate.google.com/>   
<https://www.memrise.com/>  
<http://language.tiu.ac.jp/index_e.html>  
<http://syosetu.com/>  
<http://www.tanos.co.uk/jlpt/>  
<https://www.aozora.gr.jp/index.html>  

### Books

A Guide to Japanese Grammar: A Japanese approach to learning Japanese grammar  
Mr Yong Kim  
Publisher: CreateSpace  
ISBN10: 1469968142 ISBN13: 9781469968148 Edition: Paperback; 2012-06-11

Tobira: Gateway to Advanced Japanese Learning Through Content and Multimedia (Japanese)  
Oka Mayumi   
Publisher: Kurosio Publishers  
ISBN10: 4874244475 ISBN13: 9784874244470 Edition: Tankobon Softcover; 2009

Tobira: Power Up Your Kanji (800 Basic Kanji as a Gateway to Advanced Japanese)  
Oka Mayumi  
Publisher: Kurosi  
ISBN10: 4874244874 ISBN13: 9784874244876 Edition: Tankobon Softcover; 2010

Oxford beginner's Japanese dictionary  
Jonathan Bunt  Hall, Gillian R.   
Publisher: Oxford University Press  
ISBN10: 0199298521 ISBN13: 9780199298525 Edition: (pbk.)

Japanese From Zero! 3: Proven Techniques to Learn Japanese for Students and Professionals  
George Trombley  Yukari Takenaka   
Publisher: YesJapan Corporation  
ISBN10: 0976998130 ISBN13: 9780976998136 DDC: 495 Edition: Paperback; 2006-05-01

Japanese from Zero! 4  
George Trombley  Yukari Takenaka   
Publisher: YesJapan Corporation  
ISBN10: 0989654508 ISBN13: 9780989654500 Edition: Paperback 2015-05-01

A Dictionary of Basic Japanese Grammar  
Makino, Seiichi  Michio Tsutsui  
Publisher: Japan Times  
ISBN10: 4789004546 ISBN13: 9784789004541 Edition: Paperback; 1989

A Dictionary of Intermediate Japanese Grammar  
Makino, Seiichi  Michio Tsutsui  
Publisher: Japam Times  
ISBN10: 4789007758 ISBN13: 9784789007757 Edition: Paperback; 1995-01

Dictionary of Advanced Japanese Grammar (Japanese Edition)  
Makino, Seiichi  Michio Tsutsui  
Publisher: Japan Times  
ISBN10: 4789012956 ISBN13: 9784789012959 DDC: 425 Edition: Paperback; 2008-08-30  

### Other

Kantai Collection (Game) - <http://www.dmm.com>  
Neptunia Series (Game) - <https://en.wikipedia.org/wiki/Hyperdimension_Neptunia>  
Anime - <https://myanimelist.net/animelist/Savag3blow>  
Manga & Light novels - 空ろの箱と零のマリア & 紫色のクオリア
